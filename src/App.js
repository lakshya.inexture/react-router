import React from "react";
import { Route, Routes } from "react-router-dom";
import Home from "./routes/home";
import Login from "./routes/login";
import NotFound from "./routes/notfound";
import Profile from "./routes/profile";
import { AuthProvider } from "./routes/useAuth";

import "./App.css";

function App() {
    return (
        <div className="App">
            <AuthProvider>
                <Routes>
                    <Route path="/" element={<Home />} />
                    <Route path="/profile" element={<Profile />} />
                    <Route path="/login" element={<Login />} />
                    <Route path="*" element={<NotFound />} />
                </Routes>
            </AuthProvider>
        </div>
    );
}

export default App;
