import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import Navbar from "../Navbar";
import { useAuth } from "./useAuth";

export default function Login() {
    const { user, login } = useAuth();

    const navigate = useNavigate();

    const [name, setName] = useState("");

    useEffect(() => {
        user && navigate("/profile");
    }, [user]);

    return (
        <div>
            <Navbar />
            {!user && (
                <div className="login">
                    <input
                        type="text"
                        value={name}
                        onChange={(e) => setName(e.target.value)}
                    />
                    <button onClick={() => login(name)}>Login</button>
                </div>
            )}
        </div>
    );
}
