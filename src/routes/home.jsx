import Navbar from "../Navbar";
import { useAuth } from "./useAuth";

export default function Home() {
    const { user, logout } = useAuth();
    return (
        <div className="home">
            <Navbar />
            <div className="home-content">
                <h1>Welcome {user}</h1>
                {user && <button onClick={() => logout()}>Logout</button>}
            </div>
        </div>
    );
}
