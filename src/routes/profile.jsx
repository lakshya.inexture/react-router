import Navbar from "../Navbar";
import { useAuth } from "./useAuth";
import { useNavigate } from "react-router-dom";
import { useEffect } from "react";

const Profile = () => {
    const { user } = useAuth();
    const navigate = useNavigate();

    useEffect(() => {
        !user && navigate("/login");
    }, [user]);

    const goHome = () => {
        navigate("/");
    };
    return (
        <div className="profile">
            <Navbar />
            <div className="profile-content">
                {user && (
                    <div>
                        <p>Hello, {user}</p>
                        <button onClick={goHome}>Home</button>
                    </div>
                )}
            </div>
        </div>
    );
};

export default Profile;
